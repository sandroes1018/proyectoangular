import {Directive , HostListener, HostBinding} from '@angular/core';

@Directive({
    selector: 'li[contar-clicks]'
})

export class ContarClicksDirective{
    cantidadClicks=0;
    @HostBinding('style.opacity') opacity: number= .1
    @HostListener('click',['$event.target'])onclick(btn){
        console.log('a',btn,"clickss",this.cantidadClicks++);
        this.opacity += .1;
    }
}