import { Injectable } from '@angular/core';
import  {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PruebaService {
  private data: Array<any>;
  constructor(private http:HttpClient) { }

  public GetService() : Observable<any>{

    return  this.http.get<any>("http://localhost:8080/ca/example");
    
  }
}
