import { Component, OnInit } from '@angular/core';
import { PruebaService } from './prueba.service';

@Component({
  selector: 'app-service-prueba',
  templateUrl: './service-prueba.component.html',
  styleUrls: ['./service-prueba.component.css'],
  providers: [PruebaService]
})
export class ServicePruebaComponent implements OnInit {
private data: Array<any>;
  constructor(private PruebaService:PruebaService) { }

  ngOnInit() {
    this.GetService();
  }

  private GetService():void{
    
     this.PruebaService.GetService().subscribe(res=>{
      this.data= res as Array<any>;
      console.log(this.data);
      },error=> {console.log("Error Service")} );
  }
  
}
