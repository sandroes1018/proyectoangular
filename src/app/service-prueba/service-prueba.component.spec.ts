import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicePruebaComponent } from './service-prueba.component';

describe('ServicePruebaComponent', () => {
  let component: ServicePruebaComponent;
  let fixture: ComponentFixture<ServicePruebaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicePruebaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicePruebaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
