
import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ServicePruebaComponent } from './service-prueba/service-prueba.component';

const routes: Routes=[
    {path:'',redirectTo:'/appComponent',pathMatch:'full' },
    {path:'appComponent',component:AppComponent},
    {path:'servicePrueba',component:ServicePruebaComponent}
];
@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule],
})

export class AppRoutingModule{}