import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProjectAngular';
  show=true;
  nombre:string='';
  paises:any=[
    {tamano:'mediano',cercano:1,activo:true,nombre:"Colombia"},
    {tamano:'grande',cercano:2,activo:true,nombre:"Brasil"},
    {tamano:'mediano',cercano:2,activo:true,nombre:"Argentina"},
    {tamano:'mediano',cercano:1,activo:false,nombre:"Polonia"},
    {tamano:'grande',cercano:3,activo:false,nombre:"Alemania"},
    {tamano:'mediano',cercano:1,activo:false,nombre:"Francia"}
  ]

  lat: number = 51.678418;
  lng: number = 7.809007;

  constructor(){
    setTimeout(()=>{this.show=false;},3000)
  }
  ejecutar(){
    alert("Ejecutando");
  }
}
